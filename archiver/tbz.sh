#!/usr/local/bin/bash
CURRENT_DIR=`pwd`
CURRENT_FILES=( `ls -1 $CURRENT_DIR` )

for i in "${CURRENT_FILES[@]}"; do
    echo "Start TBZing $i"
    tar cjf $i.tbz $i
    echo "End TBZing $i"
#    unrar x $i ;
done
