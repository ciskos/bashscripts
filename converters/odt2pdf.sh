#!/usr/bin/bash

for d in */; do
    cd "$d"
    echo "Working in $d"
    libreoffice --headless --convert-to pdf *.odt
    rm *.odt
    echo "Exiting $d"
    cd ..
done
