#!/usr/local/bin/bash

ARCHIVE_DIR="/home/kot/work/sync/"
COMP_ARRAY=( inessa sveta tuna irina )
#DATE_TIME_FILE=`date "+%Y-%m-%d"`
DATE_TIME_FILE=`date "+<%Y-%m-%d%>"`
#DATE_TIME_INFILE=`date "+<%Y-%m-%d% %H:%M:%S>"`

#LOGGING
LOG_HOME="/home/kot/log/oks/"
#log_files
MOUNT_LOG="oks_mount/oks_mount_${DATE_TIME_FILE}.log"
DOWNLOAD_LOG="oks_download/oks_downloaded_${DATE_TIME_FILE}.log"
ERASE_LOG="oks_erased/oks_erased_${DATE_TIME_FILE}.log"
#CP_LOG="oks_cp_`date "+%Y-%m-%d"`.log"
#log_dirs
MOUNTED="${LOG_HOME}${MOUNT_LOG}"
DOWNLOADED="${LOG_HOME}${DOWNLOAD_LOG}"
ERASED="${LOG_HOME}${ERASE_LOG}"
#CP="${LOG_HOME}${CP_LOG}"
#log_array
LOG_FILES=( ${MOUNTED} ${DOWNLOADED} ${ERASED} )

#RSYNC
RSYNC_COMPARE_DEST="/home/kot/SOK/"
RSYNC_OPTIONS="--compare-dest="${RSYNC_COMPARE_DEST}" \
               --no-implied-dirs \
			   --exclude="*2077*"
			   --exclude="*2809*"
			   --exclude="*200[0-8]*/" \
			   --exclude="*20090[1-6]*/" \
			   --exclude="*200710*/" \
               --size-only \
               --stats \
               -vr"
#MOUNT
MOUNT_GREP="mount | grep ${i}"
MOUNT_SMBFS_OPTIONS="-N -E koi8-r:cp866"

echo_log ()
{
  echo $'\n' "`date`" $'\n'  >> $j
}

rsync_func ()
{
	DATE_TIME_INFILE=`date "+<%Y-%m-%d% %H:%M:%S>"`
  echo "##################################################################" >> ${DOWNLOADED}
  echo "${DATE_TIME_INFILE}: �������� ��������� ����� � ${i}" >> ${DOWNLOADED}
  echo "##################################################################" >> ${DOWNLOADED}
  rsync ${RSYNC_OPTIONS} /mnt/smb/${i}/ ${ARCHIVE_DIR} >> ${DOWNLOADED}
  echo "__________________________________________________________________" >> ${DOWNLOADED}
  echo "${DATE_TIME_INFILE}: ��������� ������� ������ � ${i}" >> ${DOWNLOADED}
  echo "==================================================================" >> ${DOWNLOADED}
}

mount_func ()
{
  echo "${DATE_TIME_INFILE}: ��������� ���� ${i}" >> ${MOUNTED}
  mount_smbfs ${MOUNT_SMBFS_OPTIONS} //konstantin@${i}/SOK$ /mnt/smb/${i}/
  if ( mount | grep ${i} > /dev/null ); then
    echo "${DATE_TIME_INFILE}: ���� ${i} ����������� �������" >> ${MOUNTED};
    else
     echo "${DATE_TIME_INFILE}: �������� ��� ������������ ����� ${i} �������� �� �� ��� ���� ����" >> ${MOUNTED}
     exit 1;
  fi
}

find_func ()
{
	DATE_TIME_INFILE=`date "+<%Y-%m-%d% %H:%M:%S>"`
  echo "##################################################################" >> ${ERASED}
  echo "${DATE_TIME_INFILE}: ������� ������ ���������� � ����� ��������� �  ${i}" >> ${ERASED}
  echo "##################################################################" >> ${ERASED}
  find ${ARCHIVE_DIR} -type d -empty -print -delete  >> ${ERASED}
  echo "__________________________________________________________________" >> ${ERASED}
  echo "${DATE_TIME_INFILE}: ��������� �������� ������ ���������� � ������ � ${i}" >> ${ERASED}
  echo "==================================================================" >> ${ERASED}
}

# cp_func ()
# {
#   echo "##################################################################" >> ${CP}
#   echo "${DATE_TIME_INFILE}: �������� ���������� � ����� ��������� �  ${i}" >> ${CP}
#   echo "##################################################################" >> ${CP}
#   cp -Rfv ${ARCHIVE_DIR} ${RSYNC_COMPARE_DEST} >> ${CP}
#   echo "__________________________________________________________________" >> ${CP}
#   echo "${DATE_TIME_INFILE}: ��������� ����������� ���������� � ������ � ${i}" >> ${CP}
#   echo "==================================================================" >> ${CP}
# }

ping_func ()
{
	DATE_TIME_INFILE=`date "+<%Y-%m-%d% %H:%M:%S>"`
  for i in ${COMP_ARRAY[@]}; do
    if ( mount | grep ${i} ); then
      echo "${DATE_TIME_INFILE}: ���� ${i} ��� �����������" >> ${MOUNTED}
      rsync_func
      find_func;
    else
      if ( ping -qo -c 1 ${i} > /dev/null ); then
        mount_func
        rsync_func
        find_func;
      else
        echo "${DATE_TIME_INFILE}: ��������� ${i} �� ��������" >> ${MOUNTED};
      fi;
    fi;
  done
}

for j in ${LOG_FILES[@]}; do
  echo_log $j;
done

ping_func
